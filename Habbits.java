package org.abhishek.insurance;

public class Habbits {
	private boolean smoking;
	private boolean alcohol;
	private boolean drugs;
	private boolean dailyExercise;
	public Habbits() {
		// TODO Auto-generated constructor stub
	}
	public Habbits(boolean smoking, boolean alcohol, boolean drugs, boolean dailyExercise) {
		super();
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.drugs = drugs;
		this.dailyExercise = dailyExercise;
	}
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	public boolean isDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	@Override
	public String toString() {
		return "Habbits [smoking=" + smoking + ", alcohol=" + alcohol + ", drugs=" + drugs + ", dailyExercise="
				+ dailyExercise + "]";
	}
	

}
