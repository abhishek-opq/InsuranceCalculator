package org.abhishek.insurance;

public class CurrentHealth {
	private boolean hypertension;
	private boolean bloodPressure;
	private boolean overWeight;
	private boolean bloodSuger;
	public CurrentHealth() {
		// TODO Auto-generated constructor stub
	}
	public CurrentHealth(boolean hypertension, boolean bloodPressure, boolean overWeight, boolean bloodSuger) {
		super();
		this.hypertension = hypertension;
		this.bloodPressure = bloodPressure;
		this.overWeight = overWeight;
		this.bloodSuger = bloodSuger;
	}
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isOverWeight() {
		return overWeight;
	}
	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}
	public boolean isBloodSuger() {
		return bloodSuger;
	}
	public void setBloodSuger(boolean bloodSuger) {
		this.bloodSuger = bloodSuger;
	}
	@Override
	public String toString() {
		return "CurrentHealth [hypertension=" + hypertension + ", bloodPressure=" + bloodPressure + ", overWeight="
				+ overWeight + ", bloodSuger=" + bloodSuger + "]";
	}
	

}
