package org.abhishek.insurance;

public class Customer {
	private String name;
	private String gender;
	private int age;
	private CurrentHealth currentHealth;
	private Habbits habbits;
	
	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(String name, String gender, int age, CurrentHealth currentHealth, Habbits habbits) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.currentHealth = currentHealth;
		this.habbits = habbits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}

	public Habbits getHabbits() {
		return habbits;
	}

	public void setHabbits(Habbits habbits) {
		this.habbits = habbits;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", gender=" + gender + ", age=" + age + ", currentHealth=" + currentHealth
				+ ", habbits=" + habbits + "]";
	}

	}
