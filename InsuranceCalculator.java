package org.abhishek.insurance;

public class InsuranceCalculator {


	private static double calculateAmount(double baseAmount, double percent) {

		return baseAmount + baseAmount * percent / 100;

	}

	public static double calculateInsurance(Customer customer) {

		double basePremium = 5000.0;

		int age = customer.getAge();
		String gender = customer.getGender();
		String name = customer.getName();
		CurrentHealth currentHealth = customer.getCurrentHealth();
		Habbits habbit = customer.getHabbits();

		/*boolean ishypertension = currentHealth.isHypertension();
		boolean isbloodPressure = currentHealth.isBloodPressure();
		boolean isoverWeight = currentHealth.isOverWeight();
		boolean isbloodSuger = currentHealth.isBloodSuger();

		boolean issmoking = habbit.isSmoking();
		boolean isalcohol = habbit.isAlcohol();
		boolean isdrugs = habbit.isDrugs();
		boolean isdailyExercise = habbit.isDailyExercise();*/

		basePremium = calculateBasePremBasedOnAge(age, basePremium);

		if (gender.equalsIgnoreCase("Male")) {
			basePremium = calculateAmount(basePremium, 2);
		}

		basePremium = calculateInsuranceBasedOnHealth(currentHealth, basePremium);

		basePremium = calculateInsuranceBasedOnHabbit(habbit, basePremium);

		return basePremium;
	}

	private static double calculateBasePremBasedOnAge(int age, double basePremium) {
		double finalPremium = basePremium;
		boolean shouldGoAhead = true;

		if (age < 18) {
			finalPremium = basePremium;
			shouldGoAhead = false;
		}

		if (age > 18 && age < 25) {
			finalPremium = calculateAmount(basePremium, 10);
			shouldGoAhead = false;
		}

		if (shouldGoAhead) {
			for (int i = 20; i < 100; i = i + 5) {
				if (age > i && i <= 40) {
					finalPremium = calculateAmount(basePremium, 10);
					basePremium=finalPremium;
				}
				if (age > i && i > 40) {
					finalPremium = calculateAmount(basePremium, 20);
					basePremium=finalPremium;
				}

			}
		}

		return finalPremium;

	}

	private static double calculateInsuranceBasedOnHealth(CurrentHealth currentHealth, double basePremium) {
		double finalPremium = basePremium;

		boolean ishypertension = currentHealth.isHypertension();
		boolean isbloodPressure = currentHealth.isBloodPressure();
		boolean isoverWeight = currentHealth.isOverWeight();
		boolean isbloodSuger = currentHealth.isBloodSuger();
		if (ishypertension) {
			finalPremium = calculateAmount(basePremium, 1);
			basePremium=finalPremium;
		}
		if (isbloodPressure) {
			finalPremium = calculateAmount(basePremium, 1);
			basePremium=finalPremium;
		}
		if (isoverWeight) {
			finalPremium = calculateAmount(basePremium, 1);
			basePremium=finalPremium;
		}
		if (isbloodSuger) {
			finalPremium = calculateAmount(basePremium, 1);
			basePremium=finalPremium;
		}

		return finalPremium;
	}

	private static double calculateInsuranceBasedOnHabbit(Habbits habbit, double basePremium) {
		double finalPremium = basePremium;
		boolean issmoking = habbit.isSmoking();
		boolean isalcohol = habbit.isAlcohol();
		boolean isdrugs = habbit.isDrugs();
		boolean isdailyExercise = habbit.isDailyExercise();
		if (issmoking) {
			finalPremium = calculateAmount(basePremium, 3);
			basePremium=finalPremium;
		}
		if (isalcohol) {
			finalPremium = calculateAmount(basePremium, 3);
			basePremium=finalPremium;

		}
		if (isdrugs) {
			finalPremium = calculateAmount(basePremium, 3);
			basePremium=finalPremium;

		}
		if (isdailyExercise) {
			finalPremium = calculateAmount(basePremium, -3);
			basePremium=finalPremium;

		}

		return finalPremium;
	}

}
